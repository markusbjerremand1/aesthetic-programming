
let speech;
let awarenss; 
let feeling; 
let thinking; 
let being; 
let iterationState; 
let iterationFeel; 
let iterationThink; 
let iterationBe; 
let start = 0; 
let end; 
let f;
let textgrow;
function preload()  {
  awareness = loadJSON("state.json"); 
  f = loadFont("CascadiaCode.ttf")
}
function setup()  {
  frameRate(60);
  createCanvas(400,400);
  // noCanvas();
  speech = new p5.Speech(); 
  speech.onLoad = voiceReady;
  speech.setVolume(0.2);
  speech.setRate(0.2);
  speech.setPitch(0.5);
  speech.setVoice('Google US English');


  iterationState = 0;  
  iterationFeel = floor(random(0,9)); 
  iterationThink = floor(random(0,9)); 
  iterationBe = floor(random(0,9)); 
  textgrow = 10;

function voiceReady() {
  console.log(speech.voices);
}
}

function draw() {
  background(0);
  let state = awareness.stateOfConsciousness; 
  textAlign(CENTER, CENTER);
  textFont("f",textgrow);
  fill(255);
  feeling = state[iterationState].iFeel; 
  thinking = state[iterationState].iThink;//
  being = state[iterationState].iAm; //
  console.log(iterationFeel);
  console.log(iterationThink);
  console.log(iterationBe);
  if (textgrow >= 40) {
    textgrow = 10;
  }
  text("i feel" + " " + feeling[iterationFeel] + "\n" + "i think" + " " + thinking[iterationThink] + "\n" + "i am" + " " + being[iterationBe], width/2, height/2); 
  if (frameCount%600 == 0) { 
    iterationFeel = floor(random(start,end));
    iterationThink = floor(random(start,end));
    iterationBe = floor(random(start,end));
    textgrow += floor(random(start,end));
    speech.speak("i feel" + feeling[iterationFeel] + " i think" + thinking[iterationThink] + " i am" + being[iterationBe]); 
  }
  if (frameCount < 3599) {
    end = 9;
  }
  if (frameCount >= 3599) {
    iterationState = 1;
    end = 7;
  }
  if (frameCount >= 7199)  {
    iterationState = 2;
    end = 6;
  }
  if (frameCount >= 10799)  {
    overload();
  }


}
function overload() {
clear();
background(0);
speech.setVolume(0.0);
textAlign(CENTER);
textFont("f", 80);
fill(255);
text("i feel" + "\n" + "i think" + "\n" + "i am", width/2, height/2);
}
