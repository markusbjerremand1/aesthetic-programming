# Minix5 - "Generative Sounds"

![](minix5.JPG)

For this weeks minix, i have created a remixed (kinda) version of 10print. However, it differs from the original code by having both a visual component and an audio component, in addition to a "timing" component. Furthermore, the visual component has been tweaked with two more "rules". The rules for the visual component are the same as those implemented for the audio component of the programme.

runme: https://markusbjerremand1.gitlab.io/aesthetic-programming/minix5/

code: https://gitlab.com/markusbjerremand1/aesthetic-programming/-/blob/master/minix5/sketch.js

# How it works (on a technical level)
The programme works through a set of four rules.
These rules are the following

1. if (random(1) < 0.25)
2. else if (random(1) > 0.25 && random(1) < 0.50)
3. else if (random(1) > 0.50 && random(1) < 0.75)
4. else (meaning if random(1) is higher than .75)

First, a quick rundown on how the rules affect the each of the components.

**Visual component**
The rules, as seen above, dictate which line will be "printed". There are for different lines. The first being a backward slash (\) if random(1) < 0.25, the second being a forward slash (/) if random(1) has a value between 0.25 and 0.5, the third being a hyphen (-) if random(1) has a value between 0.5 and 0.75, and the last being a straight line (|) if random(1) has a value over 0.75. This creates the entralling pattern that emergens once the program has been allowed to run for a while.

**Audio component**
The rules, as stated, are exactly the same for the audio component. However, instead of dictating which line will be printed, they now dictate the wavetype of the p5.Oscillator. These wavetypes are sine, triangle, sawtooth and square. Futhermore, when each of these are executed their amplitude and frequency will be randomly selected. For amplitude, it will be a random value between 0 and 1. For the frequence, a value between 0 and 300. 

**Timing component**
Again, the rules are the same. However, this time the random number generated from random(1) will dictate the framerate which the program will run with. If random(1) < 0.25, the framerate will be a value between 1 and 15. If random(1) has a value between 0.25 and 0.5, the value will be between 1 and 30, and so on.
The timing component was added as a way of experimenting with the types of sounds or soundtracks for generativity could be created, and if it would be able to create random parts of sounds that could constitute what we might call "listenable music" (i've yet to experience this myself, though).

# The Role of Control
The rules, and what they mean for both autonomy and control, are quite interesting in the programme (in my own opinion). One the one hand, the rules are hinged upon randomness, and as such, there is not "full" control over the programme. It is autonomous, to some degree. However, there is at the same time something intoxicating about having created a function that randomizes and shapes the time in the programme. As the programmer, i created this function; the function in turn affects the time of the programme. So, even though i am not directly affecting the time, i have still created that which is currently doing so. It is quite a clichée to draw upon the analogy of the programmer as a "god of his own realm", however, in this sense the analogy begins to make sense. Just as god created man (according to some), gave them rules (commandments) and then left them alone to execute these rules they had been given, it is not far out of reach to draw a parallel. However, the programmer as being a type of god may potentially be a dangerous idea, so i won't elaborate further, besides, saying again, that this control can feel intoxicating (when it works as intended, otherwise it can be a sour and disheartening experience).

However, this naturally raises the question of whether computational autonomy is even something that exists. The programme runs as it does, because i gave it rules to follow. Maybe i didn't explicitly tell it all that it needed to do, but the rules guided it to some sort of goal. However, autonomy might also simply be present in the ability to follow rules. If it didn't have atleast some sense of autonomy, it would have to be told explicitly what to do at every step of the way. 
