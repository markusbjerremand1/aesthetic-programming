let l1x = 100;
let l1y = 100;
let l1Spacing = 15;
let timing = 1;

let ampOsc;
let freqOsc;
let waveType;

function setup() {
  createCanvas(1000, 1000);
  background(0);

  osc = new p5.Oscillator();

}

function draw() {
frame();
time();
frameRate(timing);
visual();
audio();
  }

function time()  {
  if (random(1) < 0.25) {
    timing = 0 + floor(random(1, 15));
  } else if (random(1) > 0.25 && random(1) < 0.50)  {
    timing = 0 + floor(random(1, 30));
  } else if (random(1) > 0.50 && random(1) < 0.75)  {
    timing = 0 + floor(random(1, 45));
  } else {
    timing = 0 + floor(random(1, 60));
  }
  if (timing > 60) {
    timing = 0;
  }
}

function visual() {
  let r = random(255);
  let g = random(255);
  let b = random(255);
 stroke(r, g, b);
 strokeWeight(1.5);
 if (random(1) < 0.25) {
   line(l1x, l1y, l1x+l1Spacing, l1y+l1Spacing);
   speed = 0 + floor(random(60));
 } else if (random(1) > 0.25 && random(1) < 0.50)  {
   line(l1x, l1y+l1Spacing, l1x+l1Spacing, l1y);
   speed = 0 + floor(random(60));
 } else if (random(1) > 0.50 && random(1) < 0.75)  {
   line(l1x, l1y, l1x+l1Spacing, l1y);
   speed = 0 + floor(random(60));
 } else {
   line(l1x, l1y, l1x, l1y+l1Spacing)
   speed = 0 + floor(random(60));
 }
 l1x+=15;
 if (l1x >= width-100) {
   l1x = 100;
   l1y += l1Spacing;
 }
}

function audio()  {
  osc.setType(waveType);
  osc.amp(ampOsc);
  osc.freq(freqOsc);
  osc.phase(0.5);
  osc.start();
  if (random(1) < 0.25) {
    waveType = 'sine';
    ampOsc = random(1);
    freqOsc = random(300);
  } else if (random(1) > 0.25 && random(1) < 0.50)  {
    waveType = 'triangle';
    freqOsc = random(300);
    ampOsc = random(1);
  } else if (random(1) > 0.50 && random(1) < 0.75)  {
    waveType = 'sawtooth';
    ampOsc = random(1);
    freqOsc = random(300);
  } else {
    waveType = 'square';
    ampOsc = random(1);
    freqOsc = random(300);
  }

}

function frame()  {
stroke(255);
strokeWeight(5);
line(0, 0, 90, 90);
line(910, 90, 1000, 0);
line(0, 1000, 90, 910);
line(910, 910, 1000, 1000);
noFill();
rect(90, 90, 820, 820);
// for (var x = 90; x < 910; x + 16) {
//   noFill();
//   stroke(255);
//   strokeWeight(5);
//   rect(x, 0, 16, 16);
//   }
//this for-loop causes crashing - hence why there is no other for loop present in the code
}
