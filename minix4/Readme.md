# "Behind the Screen: Mirror Image" - MiniX4
![Alternative Text](https://media.giphy.com/media/LTSo0upeXackNsgiCS/giphy.gif)

RunMe: https://markusbjerremand1.gitlab.io/aesthetic-programming/minix4/

Code: https://gitlab.com/markusbjerremand1/aesthetic-programming/-/blob/master/minix4/sketch.js
# Short description of the work
As the title of the work suggests, 'Behind the Screen: Mirror Image', the main concept
behind it starts with the simple question: "When we use a programme that captures our faces,
then what happens behind the screen? What happens to these images, this data, that lay at the core of our identity?".

This question is explored in this work, using the face as a starting point for what constitues
the identity of the individual, in an attempt to provide a possible answer to what may happen
with this facial data when we use these types of programmes, and why we ought to be wary when
and how we might use them, seeing as we do not know what these images might be used for.

The work is, in itself, quite simple. A video of the user is captured, whereupon their face
is tracked. The captured video is hidden, and instead three symbols are mapped to the tracking
points of their eyes and mouth. These symbols change when the user makes noise, either by clapping,
talking or shouting, further encouraging the user to continue the use of the app in order to see
what other combinations of symbols might be possible, discovering new possible "emojis" or "emoticons".

This could indeed be a free, sketchy app that someone might download on either the Appstore or
Google Play. However, in this programme, the processes happening behind the screen are not hidden.
When the user makes noise, several images of them will appear behind the "emoji". These are snapshots
of the captured video, and as such, of the user. These pictures are not erased, stressing the entire
point behind the work: As soon as our faces have been captured, then that data is unerasable. These data
all help create a trail leading back to us as users, as examplified in the work as the background is placed
in the setup function instead of the draw function; the background will not erase or cover up this data.
Instead, it will continue to accumulate until we finally stop using the programme, and by that time, it is
already too late. As much as we were told an app was free, as much were we lied too; we did pay for using it.
Just not with money. We pay with our data, parts of our identity. We are the product.

# Description of the programme
For this programme i have used bot audio and video capture, coupled with the clmtracker library
in order to track the face of the user and then add the symbols to the eyes and mouth. Furthermore,
in order to make it possible to have both the snapshots of the capture not be covered by the background
and the symbols being covered up (such as to not leave a trail when the user moves their head),
i used createGraphics to create an "extra" canvas whereupon i was able to draw the snapshots,
without the background of the main canvas covering them up. This truly was a lifesaver.
Otherwise, the map()function has been used to map the volume of the microphone to different
parameters of objects, such as size, as well as a condition for if-statements, getting activated
when the volume was over 0.01. This i used to let the snapshots start to appear in the background 
when the user makes noise.

# "Capture All"
In short, i will say the same as in the short description of the work.
When each aspect of what we do increases the chance of our information being captured, whether that
be video or audio capture, or our current location through GPS-tracking or similar, it is 
increasingly important to be weary of what types of programmes we use, how we use them and
why we use them. Especially since what is captured about us is unerasable, impossible to revoke.
And probably worst of all is, that we most likely agreed that they could take whatever they wanted from us.
