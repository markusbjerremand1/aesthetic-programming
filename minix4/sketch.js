let capture;
let capture2;
let mic;

let ctracker;
let eye1Options = ['0', 'O', 'o', 'q', 'Q', 'x', 'X', '+', '*', '-', '_', ',', '~', '!', '@', '#', '£', '€', '$', '¢', '¥', '§', '%', '°', '^', '&', '(', ')', '=', '{', '}', '[', ']', '|', '"', '<', '>', '?', 'Z', 'z', '¤', 'V', 'v', 'U', 'u', 'Y', 'y', 'T', 't'];
let eye2Options = ['0', 'O', 'o', 'q', 'Q', 'x', 'X', '+', '*', '-', '_', ',', '~', '!', '@', '#', '£', '€', '$', '¢', '¥', '§', '%', '°', '^', '&', '(', ')', '=', '{', '}', '[', ']', '|', '"', '<', '>', '?', 'Z', 'z', '¤', 'V', 'v', 'U', 'u', 'Y', 'y', 'T', 't'];
let mouthOptions = ['0', 'O', 'o', 'q', 'Q', 'x', 'X', '+', '*', '-', '_', ',', '~', '!', '@', '#', '£', '€', '$', '¢', '¥', '§', '%', '°', '^', '&', '(', ')', '=', '{', '}', '[', ']', '|', '"', '<', '>', '?', 'Z', 'z', '¤', 'V', 'v', 'U', 'u', 'Y', 'y', 'T', 't'];
//48 SYMBOLS - 110.592 combinations/ emoticons
let indexEye1 = 0;
let indexEye2 = 0;
let indexMouth = 0;

let pg;
let button;
function setup()  {
//video
createCanvas(2000, 2000);
capture = createCapture(VIDEO);
capture.size(2000, 2000);
capture.hide();
capture2 = createCapture(VIDEO);
capture2.size(320, 240);
capture2.hide();
// audio
mic = new p5.AudioIn();
mic.start();
//ctracker
ctracker = new clm.tracker();
ctracker.init(pModel);
pg = createGraphics(2000, 2000);
//button
button = createButton('Press to start, and try to make some noise');
button.position(0, 0);
button.size(2000, 2000);
button.style('font-size', '100px');
button.style('color', '#ffffff')
button.style('background-color', '#000000');
button.style('font-style', 'Helvetica');
button.mousePressed(startLoad);
function startLoad()  { //when button is pressed
  button.hide();
  userStartAudio();
  ctracker.start(capture.elt);
  }
}

function draw() {
let vol = mic.getLevel();
background(0);
let num = 0 + int(random(1, 48));
let num2 = 0 + int(random(1, 48));
let num3 = 0 + int(random(1, 48));

let eye1Text = eye1Options[indexEye1]
let eye2Text = eye2Options[indexEye2]
let mouthText = mouthOptions[indexMouth]

//trails
let x = random(0, 2000);
let y = random(0, 2000);
let w = map (vol, 0, 1, 160, 480);
let h = map (vol, 0, 1, 120, 360);
let r = random(255);
let b = random(255);
let g = random(255);
let textR = 255;
let textG = 255;
let textB = 255;
if (vol > 0.01)  {
  push();
  pg.tint(r, g, b);
  pg.image(capture2, x, y, w, h);
  pg.tint(r, g, b);
  pg.image(capture2, x, y, w, h);
  pg.tint(r, g, b);
  pg.image(capture2, x, y, w, h);
  pg.tint(r, g, b);
  pg.image(capture2, x, y, w, h);
  pop();
  //left eye
  indexEye1 = indexEye1 + num;
  //right eye
  indexEye2 = indexEye2 + num2;
  //mouth
  indexMouth = indexMouth + num3;
  textR = random(255);
  textG = random(255);
  textB = random(255);
}
image(pg, 0, 0); //extra canvas from pg (createGraphics)
  // no trails
  let positions = ctracker.getCurrentPosition();

  let mouthSize = map(vol, 0, 1, 800, 1200);

  if (positions.length) {
  //left eye
     let eye1x = (positions[27][0]-250);
     let eye1y = (positions[27][1]+20);
     fill(255);
     textSize(800);
     textAlign(CENTER);
     fill(textR, textG, textB);
     text(eye1Text, eye1x, eye1y);
     if (indexEye1 > 48) {
       indexEye1 = 0;
     }
     //right eye
     let eye2x = (positions[32][0]+250);
     let eye2y = (positions[32][1]+20);
     textSize(800);
     text(eye2Text, eye2x, eye2y);
     if (indexEye2 > 48) {
       indexEye2 = 0;
     }
     //mouth
     let mouthx = (positions[57][0]);
     let mouthy = (positions[57][1]+600);
     textSize(mouthSize);
     text(mouthText, mouthx, mouthy);
     if (indexMouth > 48) {
       indexMouth = 0;
     }
  }
}
