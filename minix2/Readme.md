# MiniX2
![](MiniX2.JPG)
"Composited Emotions"
- or combining digital symbols to show possible human emotions (alternative "emojis")

RunMe: https://markusbjerremand1.gitlab.io/aesthetic-programming/minix2/

For this MiniX, i have created a simple program that combines three variable digital elements in order to create an emotive "face". This face consists of 3 elements; a left eye, a right eye and a mouth. These three "human" elements were chosen, seeing as traditional emojis most often use these elements to show some type of emotion (and, in my opinion, the eyes and the mouth are some of the most emotive features in our faces; however, only using these elements naturally means that some nuances are lost in translation).

These three elements take the shape of symbols that can be found on standard QWERTY-keyboards. Each element can take the shape of one of 48 different symbols, which means that there are precisely 110.592 different combinations/possible "emojis".

![Alternative Text](https://media.giphy.com/media/YPbUGHvR1yEVyZTLq9/giphy.gif) The program always starts with the three elements using the same symbol. From here, it is possible to change each individual element by simply clicking on them.

However, there is also another option (which is recommended). Simply just press the "randomize" button. Clicking this button will result in the program displaying a random symbol for each element, leading to some interestingly useable and higly emotive (albeit abstract) "emojis", and some that are most definitely not.

![Alternative Text](https://media.giphy.com/media/GW4UT7gtRxca2PaRcG/giphy.gif) - "Randomizer"

Throughout this assignment, i have especially used the concepts of "Arrays" to list the different symbols for each element. As can be seen in the code, whenever an element is pressed, the value of the element's index goes up by one (indexEye1 = indexEye1 + 1;) - for example. The same principle has been used to "randomize" the elements; however, instead of adding one to the currente value, a random integer between 1-48 is added to the currente value of the index. Furthermore, please ignore the three first variables (eye1, eye2 and mouth) declared just after background in function draw - these were from a previous version of the program, and serve no purpose to the current version.

The purpose of this program is exploratory; in what other ways can emotions be shown digitally, as an alternative to "traditional" emojis?
As such, the purpose of the "emojis" created by the program isn't to be something that someone can identify with. The "emojis" aren't meant to be representative, other than represent an emotion that someone might be presently experiencing, and wanting to share. 
In this sense, the "composited emotions" attempts to "represent" every user as being alike, by representing their emotions through the same three-element composition, that abstractly seeks to define what they all have in common; they are all human.

As such, this program is an attempt to try and think about how else we can show emotions digitally, especially when put into a wider social- and cultural context, where "traditional" emojis have become things which we identify with. As "personalization" of emojis continues (bringing forth with it more cries for representation), it seems that an increasing amount of our identity, when we communicate our emotions digitally, is related to the emojis that we use. So, what would happen if almost every identifiable or relatable trait about an emoji was removed, only showing the users current emotion, and what different "computed emotions" would we be able to show through various symbols found on our keyboard?

I don't know, but maybe the program can show some possibilities for alternative digital "emojis" (if you just press "randomize" enough times).



