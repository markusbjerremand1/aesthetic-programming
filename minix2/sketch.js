
let eye1Options = ['0', 'O', 'o', 'q', 'Q', 'x', 'X', '+', '*', '-', '_', ',', '~', '!', '@', '#', '£', '€', '$', '¢', '¥', '§', '%', '°', '^', '&', '(', ')', '=', '{', '}', '[', ']', '|', '"', '<', '>', '?', 'Z', 'z', '¤', 'V', 'v', 'U', 'u', 'Y', 'y', 'T', 't'];
let eye2Options = ['0', 'O', 'o', 'q', 'Q', 'x', 'X', '+', '*', '-', '_', ',', '~', '!', '@', '#', '£', '€', '$', '¢', '¥', '§', '%', '°', '^', '&', '(', ')', '=', '{', '}', '[', ']', '|', '"', '<', '>', '?', 'Z', 'z', '¤', 'V', 'v', 'U', 'u', 'Y', 'y', 'T', 't'];
let mouthOptions = ['0', 'O', 'o', 'q', 'Q', 'x', 'X', '+', '*', '-', '_', ',', '~', '!', '@', '#', '£', '€', '$', '¢', '¥', '§', '%', '°', '^', '&', '(', ')', '=', '{', '}', '[', ']', '|', '"', '<', '>', '?', 'Z', 'z', '¤', 'V', 'v', 'U', 'u', 'Y', 'y', 'T', 't'];
//48 SYMBOLS - 110.592 combinations/ emoticons
let indexEye1 = 0;
let indexEye2 = 0;
let indexMouth = 0;

function setup()  {
  createCanvas(400, 600);
  frameRate(10);
}

function draw() {
  background(random(230, 240), (200,100), (200, 240));

  let eye1 = random(eye1Options);
  let eye2 = random(eye2Options);
  let mouth = random(mouthOptions);

  //canvas for emoticon
  fill(0);
  noStroke();
  rect(0, 0, 400, 600);

  //left eye
  push();
  textSize(200);
  fill(255);
  translate(125, 300);
  textAlign(CENTER);
  text(eye1Options[indexEye1], 0, 0);
  pop();
  //for left eye / eye1
  if (mouseX > 45 && mouseX < 200 && mouseY > 135 & mouseY < 345) {
    //mouseover
    noFill();
    stroke(255);
    rect(45, 135, 155, 210);
    if (mouseIsPressed) {
    indexEye1 = indexEye1 + 1;
  }
  }
  if (indexEye1 >= 49) {
    indexEye1 = 0;
  }

  //right eye
  push();
  textSize(200);
  fill(255);
  translate(275, 300);
  textAlign(CENTER);
  text(eye2Options[indexEye2], 0, 0);
  pop();
  //for right eye / eye2
  if (mouseX > 200 && mouseX < 355 && mouseY > 135 & mouseY < 345) {
    //mouseover
    noFill();
    stroke(255);
    rect(200, 135, 155, 210);
    if (mouseIsPressed) {
    indexEye2 = indexEye2 + 1;
  }
  }
  //limit - resets array (array 0)
  if (indexEye2 >= 49) {
    indexEye2 = 0;
  }

  //mouth
  push();
  textSize(200);
  fill(255);
  translate(200, 500);
  textAlign(CENTER);
  text(mouthOptions[indexMouth], 0, 0);
  pop();
  //for mouth
  if (mouseX > 122.5 && mouseX < 275 && mouseY > 345 & mouseY < 555) {
    //mouseover
    noFill();
    stroke(255);
    rect(122.5, 345, 155, 210);
    if (mouseIsPressed) {
    indexMouth = indexMouth + 1;
  }
  }
  //limit - resets array (array 0)
  if (indexMouth >= 49) {
    indexMouth = 0;
  }

  //randomizer
  fill(255);
  text('randomize:', 315, 533)
  let num = 0 + int(random(1, 48));
  let num2 = 0 + int(random(1, 48));
  let num3 = 0 + int(random(1, 48));
  stroke(255);
  noFill();
  rect(380, 520, 20, 20);
  if (mouseX > 380 && mouseX < 400 && mouseY > 520 & mouseY < 540) {
    fill(255);
    rect(380, 520, 20, 20);
    if (mouseIsPressed) {
      //left eye
      indexEye1 = indexEye1 + num;
      //right eye
      indexEye2 = indexEye2 + num2;
      //mouth
      indexMouth = indexMouth + num3;
    }
    }
}
