let capture;
let ctracker;
let emotionData;
let ec;

let happy = [];
let angry = [];
let sad = [];
let surprised = [];

let happyIndex = 0;
let angryIndex = 0;
let sadIndex = 0;
let surprisedIndex = 0;

let eye1Text;
let eye2Text;
let mouthText;

let sliderRed;
let sliderGreen;
let sliderBlue;

function setup()  {
  frameRate(45);
  createCanvas(700, 600);
  let buttonStart = createButton("Create your own emoticon - add symbols that could visualize being happy, angry, sad or surprised - then try to express any of those emotions with your face and press 'create emoticon' - the emoticon created will be based on your current emotion, and the symbols you add to visualize the emotion - press here to start PS: to see the symbols you added, open the console and type in 'happy' or any other of the four categories");
  buttonStart.position(0, 0);
  buttonStart.size(500, 600);
  buttonStart.style('font-size', '20px');
  buttonStart.style('color', '#ffffff')
  buttonStart.style('background-color', '#000000');
  buttonStart.style('font-style', 'Helvetica');
  buttonStart.mousePressed(startLoad);

  let buttonSave = createButton("save as image");
  buttonSave.position(515, 560);
  buttonSave.size(150, 25);
  buttonSave.style('color', '#ffffff')
  buttonSave.style('background-color', '#000000');
  buttonSave.style('font-style', 'Helvetica');
  buttonSave.mousePressed(saveIMG);

function startLoad()  { //when button is pressed
    buttonStart.hide();
  }
function saveIMG()  {
  saveCanvas();
}
  //video capture
  capture = createCapture(VIDEO);
  capture.size(500, 500);
  capture.hide();

  //ctracker
  ctracker = new clm.tracker();
  ctracker.init(pModel);
  ctracker.start(capture.elt);

  //emotion detection
  ec = new emotionClassifier();
  ec.init(emotionModel);
  emotionData = ec.getBlank();

  //ui
  //input box "happy"
  let inputHappy = createInput();
  inputHappy.position(515, 160);
  inputHappy.size(15, 15);
  let buttonHappy = createButton("add");
  buttonHappy.position(540, 160);
  buttonHappy.size(50, 21);
  buttonHappy.style('color', '#ffffff')
  buttonHappy.style('background-color', '#000000');
  buttonHappy.style('font-style', 'Helvetica');
  buttonHappy.mousePressed(pushHappy);

  //input box "angry"
  let inputAngry = createInput();
  inputAngry.position(515, 210);
  inputAngry.size(15, 15);
  let buttonAngry = createButton("add");
  buttonAngry.position(540, 210);
  buttonAngry.size(50, 21);
  buttonAngry.style('color', '#ffffff')
  buttonAngry.style('background-color', '#000000');
  buttonAngry.style('font-style', 'Helvetica');
  buttonAngry.mousePressed(pushAngry);

  //input box "sad"
  let inputSad = createInput();
  inputSad.position(515, 260);
  inputSad.size(15, 15);
  let buttonSad = createButton("add");
  buttonSad.position(540, 260);
  buttonSad.size(50, 21);
  buttonSad.style('color', '#ffffff')
  buttonSad.style('background-color', '#000000');
  buttonSad.style('font-style', 'Helvetica');
  buttonSad.mousePressed(pushSad);

  //input box "surprised"
  let inputSurprised = createInput();
  inputSurprised.position(515, 310);
  inputSurprised.size(15, 15);
  let buttonSurprised = createButton("add");
  buttonSurprised.position(540, 310);
  buttonSurprised.size(50, 21);
  buttonSurprised.style('color', '#ffffff')
  buttonSurprised.style('background-color', '#000000');
  buttonSurprised.style('font-style', 'Helvetica');
  buttonSurprised.mousePressed(pushSurprised);

  //slider for red
  sliderRed = createSlider(0, 255, 255);
  sliderRed.position(515, 430);
  sliderRed.style('width', '150px');

  //slider for green
  sliderGreen = createSlider(0, 255, 255);
  sliderGreen.position(515, 480);
  sliderGreen.style('width', '150px');

  //slider for blue
  sliderBlue = createSlider(0, 255, 255);
  sliderBlue.position(515, 530);
  sliderBlue.style('width', '150px');

function pushHappy()  { //taking the input of the input element and adding it to the array
  let happyInput = inputHappy.value();
  happy.push(happyInput);
  inputHappy.value('');
  console.log(happy);
  }

function pushAngry()  {
  let angryInput = inputAngry.value();
  angry.push(angryInput);
  inputAngry.value('');
  console.log(angry);
  }

function pushSad()  {
  let sadInput = inputSad.value();
  sad.push(sadInput);
  inputSad.vlaue('');
  console.log(sad);
  }

function pushSurprised()  {
  let surprisedInput = inputSurprised.value();
  surprised.push(surprisedInput);
  inputSurprised.value('');
  console.log(surprised);
  }
 }
function draw() {
  image(capture, 0, 0, 500, 500);
  background(0); //overlaying image with background colour

  //ui - white rectangle on right
  push();
  fill(255);
  rect(500, 0, 200, 600);
  pop();

  //input descriptor
  textSize(15);
  fill(0);
  textAlign(LEFT);
  text("add 'happy' symbol", 515, 155);
  text("add 'angry' symbol", 515, 205);
  text("add 'sad' symbol", 515, 255);
  text("add 'surprised' symbol", 515, 305);

  //emoticon colour - the values of the sliders decide the colour values of R, G, B
  text("value of red:", 515, 430);
  let textR = sliderRed.value();
  text("value of green:", 515, 480);
  let textG = sliderGreen.value();
  text("value of blue:", 515, 530);
  let textB = sliderBlue.value();

  //tracking for emotion detection
  let cp = ctracker.getCurrentParameters();
  let er = ec.meanPredict(cp);

  //the "create emoticon" "button"
  push();
  fill(0);
  stroke(0);
  strokeWeight(5);
  rect(515, 350, 150, 50);
  fill(255);
  textAlign(CENTER);
  text("create emoticon", 590, 380);
  pop();

  tableEmotions(); //function for table of emotions in upper right corner

  getCurrentEmotion(); //calculates and returns the dominant emotion

  emoticon(); //draws the emoticon

function emoticon() {

  let positions = ctracker.getCurrentPosition();

  if (positions.length) {
  //left eye
     let eye1x = (positions[27][0]-62);
     let eye1y = (positions[27][1]+5);
     fill(255);
     textSize(200);
     textAlign(CENTER);
     fill(textR, textG, textB);
     text(eye1Text, eye1x, eye1y);

     //right eye
     let eye2x = (positions[32][0]+62);
     let eye2y = (positions[32][1]+5);
     textSize(200);
     text(eye2Text, eye2x, eye2y);

     //mouth
     let mouthx = (positions[57][0]);
     let mouthy = (positions[57][1]+150);
     textSize(200);
     text(mouthText, mouthx, mouthy);

  }
  }

function tableEmotions()  {
  //https://gist.github.com/lmccart/532780549d49e36b5558 - code taken from here
  for (var i=0; i<er.length; i++) {
    push();
    textAlign(LEFT);
    textSize(15);
    fill(0);
    text(er[i].emotion+' '+nfc(er[i].value, 2), 520, (i+1)*30);
    pop();
}
}

function getCurrentEmotion()  {
  //https://stackoverflow.com/questions/23726448/accessing-values-in-javascript - code taken from here
    if(!ec.meanPredict(ctracker.getCurrentParameters())){setTimeout(getCurrentEmotion,1000);return;}
    var currentData = ec.meanPredict(ctracker.getCurrentParameters());
    var currentScores = [];

    //Gather all scores in an array
    for(var i=0;i<currentData.length;i++)
    {
        currentScores.push(currentData[i].value);
    }

    //Get the biggest score
    var max = Math.max.apply(null,currentScores);
    //Calculate its index
    var indexOfScore = currentScores.indexOf(max);
    //Get the associated emotion
    var emotion = currentData[indexOfScore].emotion;
    console.log(emotion);
    fill(255);
    strokeWeight(5);
    text("current emotion:", 20, 20);
    text(emotion, 20, 40);

    //if the emotion detection detects that the user is happy, and the user
    //presse the "create" emoticon, an emoticon will be made on the basis
    //of the symbols the user has added to the array of said emotion
    if (mouseX > 515 && mouseX < 665 && mouseY > 350 && mouseY < 400) {
      fill(textR, textB, textG);
      rect(515, 350, 150, 50);
      fill(0);
      textAlign(CENTER);
      text("create emoticon", 590, 380); //the rectangle above covers the previous "create emotion" text - hence why it is added again
    if (emotion == "happy" && mouseIsPressed) {
      eye1Text = random(happy);
      eye2Text = random(happy);
      mouthText = random(happy);

    } else if (emotion == "angry" && mouseIsPressed) {
      eye1Text = random(angry);
      eye2Text = random(angry);
      mouthText = random(angry);

    } else if (emotion == "sad" && mouseIsPressed) {
      eye1Text = random(sad);
      eye2Text = random(sad);
      mouthText = random(sad);

    } else if (emotion == "surprised" && mouseIsPressed) {
      eye1Text = random(surprised);
      eye2Text = random(surprised);
      mouthText = random(surprised);
    }
  }

    //Set up a loop (did not add 'var', to allow stopping it from outside)
    currentEmotionLoop = setTimeout(getCurrentEmotion,3000);
}
}
