# MiniX6
![](MiniX6.JPG) "Composited Emotions" - V2

For this assignment, i planned to rework my second MiniX, "Composite Emotions". This choice was made on the basis of re-reading the feedback i received from some of my peers (Anne & Jacob), that i thought was an incredibly valuable source for further exploration of the concept of alternative emojis, and future imaginaries wherein the creation and meaning of emojis/emoticons might be democratized to avoid the problems that arise from having organisations or corporations, with their own interests, create and ultimately deciding this for the user.
 
So, seeing as this programme has been made based on the same concept (more or less), i guess this programme could be called "Composite Emotions" - version 2.0.

RunMe: https://markusbjerremand1.gitlab.io/aesthetic-programming/minix6/

Code: https://gitlab.com/markusbjerremand1/aesthetic-programming/-/blob/master/minix6/sketch.js

**What has been changed:**
A majority of the things that have been implemented are brand new, but there are still some core aspects that have not been entirely replaced (although their functioning has been completely reworked).
The programme stil revolves around the "composited face", consisting of 3 elements; a left eye, a right eye and a mouth. I consider these elements to be some of the most emotive features in our faces (hence why a wide variety of traditional emojis/emoticons most often use these to express emotion). So, that is the reasoning for still keeping the face structure to this simple 3-element layout.
 
The proces of the programme choosing which symbol to show in each element is still build around randomness, albeit with a small (or rather major) change; instead of having an array for each element (eye1Index, eye2Index and so forth), the programme contains four different arrays, corresponding to four different emotions; 'happy', 'angry', 'sad' and 'surprised'. As such, the symbols of the elements are randomly picked from said arrays (with a twist, as will be explained in a bit). However, this is where another major change occurs; there are no values in the array when the programme is started. Instead, the user of the programme can themselves add different symbols to each of the arrays. For example, the symbol '$' could be added to the array 'happy', or the symbol '>' could be added to the array 'angry'. In this sense, the user is free to for themselves decide what symbols should be used to create the emoticon, based on their own interpretation of what a given symbol means (or what it could mean, when used to create an 'emoji' that is intended to describe their emotions and/or understanding of an emotion).
 
Now, this is where it gets interesting (and/or techinal and/or complicated and/or confusing).
In order to decide from which array the symbols should be picked, the programme utilises the clmtrackr library, combined with Audun M. Øygards code for emotion models and emotion classification (and some more code in the getCurrentEmotion function), to determine the users current emotion. If emotion detection detects that the user is happy, and the user presses the 'create emoticon' button, the symbols for the elements will be randomly picked from the array 'happy'.
As such, the emoticon created will represent happiness, as it consists of symbols the user thought to symbolize happiness in some sort of way, that they then added to the array 'happy'. The same is true for the rest of the emotions ('angry', 'sad' and 'surprised').
 
Finally, based on the feedback from Anne & Jacob, i implemented the saveArray function, meaning that users can now create their own "Composited Emotions" as a png file directly from the browser, and share or exchange them with eachother.
 
So, to briefly summarize the technical changes of the programme:
- Implementation of facetracking (clmtrackr)
- Implementation of emotion detection (clmtrackr)
- Implementation of user input of symbols ('emotion arrays')
- Implementation of saveArray, so users can download their own "Composited Emotion"

So, that was it for the technical changes. The next part will explain why these changes have been made.

**Why it has been changed:**
Just like the first iteration of this programme was an attempt to explore how else emotions could be shown digitally, especially pit into a wider social- and cultural context, where "traditional" emojis have become things we identify with, at times serving as extensions of ourselves in our daily communication. This "personalization" of emojis means that some people might not be represented while some others do - leading to uproar over lacking representation, that corporations can then monetize while proudly saying "we think representation is important".
 
More interestingly, it seems that when ever these corporations "try" to better representation in the landscape of emojis, they never quite seem to "get it right" (case and point, using the white-skinned emoji as a base template, with other skin tones being modifiers put on top) 
 
Then there is the other problem; how come that these corporations should be the ones that decide which emojis are created? Furthermore, why should they decide what a given emoji reprensents/symbolizes? How can a monopoly on communication ever be justified?
 
Exactly these questions are the major factors that prompted the rework of the programme; by letting myself, as the designer, dictate what symbols should possible to use (by predefining them in an array) i myself also partake in the monopolization of meaning, deciding what can be made and what can not. 
Furthermore, Anne & Jacob pointed out another important point that i missed when creating the first iteration of the programme;

> "In some way the program excludes some language types, like different languages using different characters…" Anne & Jacob, feedback MiniX2

All of these problems that can be ascribed to monopolization of communication, left me wondering "how might the creation, meaning and liberation of emojis/emoticons be democratized, so that these problems do not occur. It was not until i read the following quote in "The Critical Makers Reader:  (Un)learning Technology" that i began getting ideas as to what future imaginaries, where monopolization of communication is absent, are possible:

> "If technology is to improve society, it must be critically reflective and designed for the complexities of what it means to be human."  Critical Makers, p. 22

I realized that, if the creation of emojis/emoticons as a tool for communicating could ever improve society, in any profound way, it would have to accommodate for the complexities of what it means to be human. We ascribe different meanings to different things (either on a personal or cultural level), interpret things differently and have different perceptions about how things feel - so, in order for emojis/emoticons to be democratized, the making of them would have to accomodate different symbols that could mean different things in order to be inclusive. This lays as a foundation for the reasoning of the changes that have been made in the new iteration of the programme.
 
This critical approach is crucial to aesthetic programming, which is demonstrated in the work through the critical approach to how emojis are created, their meanings decided, and ultimately, monopolized as tools for communication. The work is not simply an example of aesthetic programming because i programmed something, but instead by using programming as tool, a material or as a site for reflection, to critically reflect on- or approach societal problems, such as the monopolization of tools for communication, but also for exploring future imaginaries where said monopolization is absent, and emojis/emoticons are democratized.



