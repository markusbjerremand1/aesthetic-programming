# First assignment
"Time" - portayal of time in a 3x3 setting

![Alt Text](https://media.giphy.com/media/O064wWI3Dw1t20QcG8/giphy.gif) (sped up 4x)

RunMe: https://markusbjerremand1.gitlab.io/aesthetic-programming/minix1/

For this first assignment, i have produced 3 different protrayals of time through 3 different shapes, manifesting as circles increasing in size, circles being filled and circles being drawn. As such, each of these shapes are "incomplete", with their level of completion corresponding to the current hour of the day, minute of the hour and second of the minute.

![](MiniX1_ellipse_increasing.JPG)
(1. shape: ellipse growing in size)

![](MiniX1_ellipse_being_completed.JPG)
(2. shape: filled arc increasing in degrees (angle))

![](Minix1_line_being_drawn.JPG)
(3. shape: circle being drawin)

Each shape has 3 variants; one representing the hour of the day, one representing the minute of the hour and one representing the second of the minute.
3 shapes with 3 variants each = 9 shapes in total. These shapes have been put in a 3x3 setting. Each vertical row in this grid includes 3 different shapes, corresponding to (from left to right) hours, minutes and seconds.

![](MiniX1_explanation_of_shapes_ROWS.jpg)

Left row = hours; middle row = minutes; right row = seconds

At first, it might be confusing as to what the different shapes mean (which is intentional). However, in order to actually use this 3x3 grid to tell the time, one must simply discover that each horizontal row in the grid show the current time of day.

![]( MiniX1_explanation_of_shapes_TIME.jpg)

Each horizontal row (when seen together), shows the current time of day. (left shape = hour of the day; middle shape = minute of the hour; right shape = second of the minute)

So, to summarize, i have made a (somewhat) confusing clock. It's usefulness as an actual means of telling the time is highly questionable.
However, it was never meant to be a useful clock. It was more an attempt to try and explore in what other ways time could be visualized, without resorting to numbers (or pointers, like on an analog clock). In this example, the thought behind was to try and take time, which is somewhat of an abstract concept, and instead of showing it in a way that concretizes it (like numbers or an analog-style clock), show it in a way that keeps it somewhat abstract, and by so, also somewhat confusing.

This first independent coding experience have both been informative, eye-opening and gratifying in it's own way, when a line of code finally works as intended. However, it has also been confusing, frustrating, and introduced even more questions than i had before. The latter is probably more a positive than a negative, as these questions now arise out of a place of curiousness and intrigue about what other things i can program (and how i can program them).
Almost every single line of code has been written by me, however, my extensive use of the map() function (in regards to mapping the values of time to the size and angle of these shapes to make the time of the day directly affect the appearance of the three shapes) can be tracked to Daniel Shiffman's video "Coding Challenge #74: Clock with p5.js" where i got the initial code for mapping time to the angle of an arc:
(let hourAngle = map(mn, 0, 24, 0, 360) - https://www.youtube.com/watch?v=E4RyStef-gY

At this point of programming experience, i find programming similar to learning a new language (which, i guess, it is). I am able to understand certain lines of code when i try read it, and thus i am able to write certain lines of code. Now, i find myself primarily being limited by vocabulary when it comes to writing code. In that sense, it is similar to reading and writing text. I can only write what i have read, or what i have been told how to write. It reminds me of learning how to spell - you read something, you write it down, and you try to say the word aloud to understand what it means. In the instance of programming, however, the computer does the talking for you. The computer shows what the code means (at least when immediately showing it in a browser).





