function setup() {
  // put setup code here
  createCanvas(400,400);

  print("hello world");

  angleMode(DEGREES);
}
function draw() {
  // put drawing code here
  background(0);

  let hr = hour();
  let mn = minute();
  let sc = second();

  //accumulative ellipse (AK)
  let hourScaleAK = map(hr, 0, 24, 0, 50);
  ellipse(100, 100, hourScaleAK);

  let minuteScaleAK = map(mn, 0, 60, 0, 50);
  ellipse(200, 200, minuteScaleAK);

  let secondScaleAK = map(sc, 0, 60, 0, 50);
  ellipse(300, 300, secondScaleAK);

  //fill arc (FL)
  noStroke();
  fill(255);

  //translate = the point of reference for rotation
  push();
  translate(100, 300);
  rotate(-90);
  let hourAngleFL = map(hr, 0, 24, 0, 360);
  arc(0, 0, 50, 50, 0, hourAngleFL);
  pop();

  push();
  translate(200, 100);
  rotate(-90);
  let minuteAngleFL = map(mn, 0, 60, 0, 360);
  arc(0, 0, 50, 50, 0, minuteAngleFL);
  pop();

  push();
  translate(300, 200);
  rotate(-90);
  let secondAngleFL = map(sc, 0, 60, 0, 360);
  arc(0, 0, 50, 50, 0, secondAngleFL);
  pop();

  //stroke arc (ST)
  push();
  strokeWeight(8);
  stroke(255);
  noFill();

  push();
  translate(100, 200);
  rotate(-90);
  let hourAngleST = map(hr, 0, 24, 0, 360);
  arc(0, 0, 45, 45, 0, hourAngleST);
  pop();

  push();
  translate(200, 300);
  rotate(-90);
  let minuteAngleST = map(mn, 0, 60, 0, 360);
  arc(0, 0, 45, 45, 0, minuteAngleST);
  pop();

  push();
  translate(300, 100);
  rotate(-90);
  let secondAngleST = map(sc, 0, 60, 0, 360);
  arc(0, 0, 45, 45, 0, secondAngleST);
  pop();

  pop();

  push();
  strokeWeight(2);
  stroke(255);
  noFill();

  //ellipse for stroke arc
  ellipse(100, 200, 50, 50);
  ellipse(200, 300, 50, 50);
  ellipse(300, 100, 50, 50);

  //ellipse for fill arc
  ellipse(100, 300, 50, 50);
  ellipse(200, 100, 50, 50);
  ellipse(300, 200, 50, 50);

  //ellipse for acc ellipse
  ellipse(100, 100, 50, 50);
  ellipse(200, 200, 50, 50);
  ellipse(300, 300, 50, 50);
  pop();
}
