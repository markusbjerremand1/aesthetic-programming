# "Noise" - MiniX3
![Alternative Text](https://media.giphy.com/media/eIxZ0UC0rsPwJlly2n/giphy.gif)

RunMe: https://markusbjerremand1.gitlab.io/aesthetic-programming/minix3/ (warning; sounds are very loud and the program includes flashing images)

Code: https://gitlab.com/markusbjerremand1/aesthetic-programming/-/blob/master/minix3/sketch.js

# The "noise" throbber
For this weeks MiniX, i have created a throbber consisting of a radial graph that is mapped to the current level of amplitude of a preloaded sound. The amplitude of the song playing is retrieved/recorded, and used as a value that is then mapped to the radius of a circle. The variations in amplitude creates the graph.

In other words, it graphs the volume of the sound to visualize the "noise" of backstage processes, normally hidden by throbbers in everyday life.

The recorded amplitude is also mapped to the rgb-colours, as well as the stop-point of an arc (the angle to stop the arc) in order to create several other visualizations of the noise; the arcs in the background have angles mapped to the recorded amplitude. The colour of the arcs, the screen and the graph are also mapped to the recorded amplitude.

For-loops have been used to create the background arcs, the keys on the keyboard and the radial graph itself - having 360 iterations (or degrees), using the x = r * cos(i) and y = r * sin(i) to convert each iteration (or angle/degrees) from an angle and a radius to an x- and a y-position. The radius being mapped to the array of the amplitude "creates" the visualization of these amplitude values as a radial graph.

Now, on a more conceptual level, the main idea behind this program was to explore how the processes happening backstage (i guess you could call it the _microprocessual process_ of the machine) could be visualized as a part of a throbbers design. When thinking of throbbers as having the purpose of "disguising" or "hiding" the processes happening backstage, underneath the superficial layer of the interface, the concept of "noise" sprung to mind. Why are these processes not shown to the user? An argument for not showing these processes could be, that they would serve as nothing but "noise", disrupting the users experience of using the interface. As such, covering up these processes could be seen as attempt to ensure the "satisfying and good user experience" - using the promise of "user friendliness" as a front for the real reasons why some of these processes might actually be hidden.

Furthermore, the throbbers that are most commonly encountered in "the wild" rarely tell anything about the process that is happening - other than that is happening, serving no more function that assuring the user that their input has been registered to avoid the frustration of poor feedback.

This program ventures a bit further than these other throbbers, attempting to visualize what processes are happening by giving these silent operations a "voice". Not only visualizing but also verbalizing this "noise" that is so often hidden from the user, this communication between different parts of the computer.


