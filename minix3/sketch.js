let song;
let button;
//array for amplitude
let volHistory = [];
function preload()  {
  song = loadSound('sorting_sfx.mp3');
}

function setup()  {
  createCanvas(500, 750);
  //background(0);
  angleMode(DEGREES);
  //amplitude object - measures the amplitude
  amp = new p5.Amplitude();
  button = createButton('Load');
  button.position(width/2-20, height/2-12);
  button.size(50, 30);
  button.mousePressed(startLoad)
  frameRate(60);
}

function startLoad()  { //when button is pressed
  song.play();
  button.hide();
  }

function draw() {
  //amp level is retrieved/ recorded
  let vol = amp.getLevel();
  volHistory.push(vol);

  background(150, 50, 100);

  //colour mapping - recorded amplitude is mapped to
  //all three rgb colours
  let red = map(vol, 0, 1, 0, 255);
  let green = map(vol, 0, 1, 100, 255);
  let blue = map(vol, 0, 1, 125, 255);

  let red2 = map(vol, 0, 1, 100, 255);
  let green2 = map(vol, 0, 1, 100, 255);
  let blue2 = map(vol, 0, 1, 125, 255);

  //background arcs line 45-165
  push();
  strokeWeight(10);
  stroke(red, green, blue);
  let arcx = 0;
  let arcStop = map(vol, 0, 1, 0, 360);
  for (let i = 0; i < 10; i++)  {
  arc(arcx, 50, 80, 80, -90, arcStop);
  arcx += 80;
  }
  pop();

  push();
  translate(width/2, 120);
  rotate(180);
  strokeWeight(10);
  stroke(red, green, blue);
  let arcx2 = -200;
  let arcStop2 = map(vol, 0, 1, 0, 360);
  for (let i = 0; i < 10; i++)  {
  arc(arcx2, 0, 80, 80, -90, arcStop2);
  arcx2 += 80;
  }
  pop();
  push();
  strokeWeight(10);
  stroke(red, green, blue);
  let arcx3 = 0;
  let arcStop3 = map(vol, 0, 1, 0, 360);
  for (let i = 0; i < 10; i++)  {
  arc(arcx3, 190, 80, 80, -90, arcStop3);
  arcx3 += 80;
  }
  pop();
  push();
  translate(width/2, 120);
  rotate(180);
  strokeWeight(10);
  stroke(red, green, blue);
  let arcx4 = -200;
  let arcStop4 = map(vol, 0, 1, 0, 360);
  for (let i = 0; i < 10; i++)  {
  arc(arcx4, -140, 80, 80, -90, arcStop4);
  arcx4 += 80;
  }
  pop();
  push();
  strokeWeight(10);
  stroke(red, green, blue);
  let arcx5 = 0;
  let arcStop5 = map(vol, 0, 1, 0, 360);
  for (let i = 0; i < 10; i++)  {
  arc(arcx5, 330, 80, 80, -90, arcStop5);
  arcx5 += 80;
  }
  pop();
  push();
  translate(width/2, 120);
  rotate(180);
  strokeWeight(10);
  stroke(red, green, blue);
  let arcx6 = -200;
  let arcStop6 = map(vol, 0, 1, 0, 360);
  for (let i = 0; i < 10; i++)  {
  arc(arcx6, -280, 80, 80, -90, arcStop6);
  arcx6 += 80;
  }
  pop();
  push();
  strokeWeight(10);
  stroke(red, green, blue);
  let arcx7 = 0;
  let arcStop7 = map(vol, 0, 1, 0, 360);
  for (let i = 0; i < 10; i++)  {
  arc(arcx7, 470, 80, 80, -90, arcStop7);
  arcx7 += 80;
  }
  pop();
  push();
  translate(width/2, 120);
  rotate(180);
  strokeWeight(10);
  stroke(red, green, blue);
  let arcx8 = -200;
  let arcStop8 = map(vol, 0, 1, 0, 360);
  for (let i = 0; i < 10; i++)  {
  arc(arcx8, -420, 80, 80, -90, arcStop8);
  arcx8 += 80;
  }
  pop();
  push();
  strokeWeight(10);
  stroke(red, green, blue);
  let arcx9 = 0;
  let arcStop9 = map(vol, 0, 1, 0, 360);
  for (let i = 0; i < 10; i++)  {
  arc(arcx9, 610, 80, 80, -90, arcStop9);
  arcx9 += 80;
  }
  pop();
  push();
  translate(width/2, 120);
  rotate(180);
  strokeWeight(10);
  stroke(red, green, blue);
  let arcx10 = -200;
  let arcStop10 = map(vol, 0, 1, 0, 360);
  for (let i = 0; i < 10; i++)  {
  arc(arcx10, -560, 80, 80, -90, arcStop10);
  arcx10 += 80;
  }
  pop();
  push();
  strokeWeight(10);
  stroke(red, green, blue);
  let arcx11 = 0;
  let arcStop11 = map(vol, 0, 1, 0, 360);
  for (let i = 0; i < 10; i++)  {
  arc(arcx11, 750, 80, 80, -90, arcStop11);
  arcx11 += 80;
  }
  pop();
  //screen
  push();
  translate(width/2, height/2);
  strokeWeight(8);
  stroke(255);
  fill(red2, green2, blue2);
  rectMode(CENTER);
  rect(0, 0, 300, 210, 10);
  pop();

  //radial graph
  push();
  strokeWeight(4);
  stroke(red, green, blue);
  noFill();
  translate(width/2, height/2);
  rotate(-90);
  beginShape();
  for (let i = 0; i < 360; i++) {
    let r = map(volHistory[i], 0, 1, 10, 150); //radius is based on the amplitude value
    let x = r * cos(i) //converts an angle (i) and a radius to an x-position
    let y = r * sin(i) //converts an angle (i) and a radius to an y-position
    vertex(x, y);//connects coordinates
  }
  endShape();
  pop();

  if (volHistory.length > 360)  {
    volHistory.splice(0, 2);
  }

//keyboard
translate(width/2, height/2);
noFill()
stroke(255);
strokeWeight(4);
beginShape();
vertex(-130, 160);
vertex(-134, 220)
vertex(134, 220);
vertex(130, 160);
endShape();
line(-130, 160, 130, 160);

//stand for screen
strokeWeight(3);
rect(-20, 105, 40, 55);

//keys
strokeWeight(2);
let x1 = -130
let y1 = 160
let x2 = -130
let y2 = 220
for (let i = 0; i < 20; i++)  {
  line(x1, y1, x2, y2);
  x1 += 13;
  x2 += 13;
}
let xright = 130
let yright = 160
for (let i = 0; i < 7; i++) {
  line(-130, y1, 130, yright);
  y1 += 10
  yright += 10
}

}
