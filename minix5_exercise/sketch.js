let x = 2;
let y = 2;
let spacing = 5;
let angle = 0
function setup() {
  createCanvas(windowWidth, windowHeight);
  background(0);
  angleMode(DEGREES);
}

function draw() {

  let r = random(255);
  let g = random(255);
  let b = random(255);
  stroke(r, g, b);
  strokeWeight(40);
  if (random(1) < 0.8) {
    line(x, y, x+spacing, y+spacing);
  } else {
    line(x, y+spacing, x+spacing, y);
  }
  x+=10;
  if (x > width) {
    x = 0;
    y += spacing;
  }
}
