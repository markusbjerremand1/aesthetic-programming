//level 1
class Ball	{
	constructor()	{
	this.speed = floor(random(1,5));
	this.size = 50;
	this.x = random(125, 875);
	this.y = 725;
	}

show()	{
	push();
	let r = random(255);
	let g = random(255);
	let b = random(255);
	stroke(r, g, b);
	fill(0);
	ellipse(this.x, this.y, this.size);
	pop();
}

move()	{
	this.y = this.y - this.speed;
}

collision()	{
	if (rect1X < this.x + this.size &&
	rect1X + rect1Width > this.x &&
	rect1Y < this.y + this.size &&
	rect1Y + rect1Height > this.y)
	{
	rect1X = 50;
	rect1Y = 325;
	tries++;
}
}
}


//level 2
class Pipe	{
	constructor()	{
		this.top = random(200, height/2);
		this.bottom = random(200, height/2);
		this.x = 880;
		this.w = 20;
		this.speed = 2;
	}

	show()	{
		push();
		let y = height-this.bottom;
		let r = random(255);
		let g = random(255);
		let b = random(255);
		stroke(r, g, b);
		fill(0);
		rect(this.x, 0, this.w, this.top);
		rect(this.x, y, this.w, this.bottom);
		pop();
	}

	move()	{
		this.x -= this.speed;
	}
	collision()	{
		if (rect1Y < this.top || rect1Y > height - this.bottom)	{
			if (rect1X > this.x && rect1X < this.x + this.w)	{
				rect1X = 50;
				rect1Y = 325;
				tries++;
			}
		}
	}
}

//Background
class Fan	{
	constructor()	{
		this.x = 0;
		this.y = 0;
		this.h = 700;
		this.w = 50;
		this.speed = 0.1;
		this.angle = 0;
	}
	show()	{
		push();
		noFill();
		stroke(255);
		translate(width/2, height/2);
		rectMode(CENTER);
		ellipse(this.x, this.y, this.h);
		rotate(this.angle);
		rect(this.x, this.y, this.h, this.w, 20);
		pop();
	}

	move()	{
		this.angle += this.speed;
	}
	collision()	{
		if (rect1X < this.x + this.w &&
		rect1X + rect1Width > this.x &&
		rect1Y < this.y + this.h &&
		rect1Y + rect1Height > this.y)
		{
		rect1X = 50;
		rect1Y = 325;
		tries++;
		}
	}
}

//lvl 3 - final
class Bullet	{
	constructor()	{
	this.speed = floor(random(1,5));
	this.size = 50;
	this.x = width+25;
	this.y = random(25, 675);
	}

show()	{
	push();
	let r = random(255);
	let g = random(255);
	let b = random(255);
	stroke(r, g, b);
	fill(0);
	ellipse(this.x, this.y, this.size);
	pop();
}

move()	{
	this.x = this.x - this.speed;
}

collision()	{
	if (rect1X < this.x + this.size &&
	rect1X + rect1Width > this.x &&
	rect1Y < this.y + this.size &&
	rect1Y + rect1Height > this.y)
	{
	rect1X = 50;
	rect1Y = 325;
	tries++;
	countdown = 30;
}
}
}
