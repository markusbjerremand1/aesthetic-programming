//player
let rect1X = 50;
let rect1Y = 325;
let rect1Width = 50;
let rect1Height = 50;
let move = 5;
offset = 25;

//Background
let fan;
let goal = 'goal';
//UI
let tries = 0;
let level = 1;

//level1
let ballnum	= 15;
let ball = [];

//level2
let pipes = [];

//level3
let timer = 5;
let countdown = 30;
let bulletnum = 20;
let bullet = [];
let content1 = 'SURVIVE FOR 30 SECONDS TO WIN';

function setup()	{
	createCanvas(1000, 700);
	stroke(255);
	pipes.push(new Pipe());
	fan = new Fan();

}
function draw()	{
	background(0);
	push();
	rectMode(CENTER);
	fill(0);
	stroke(255);
	safepoints(); //creating start- and end zones
	rect(rect1X, rect1Y, rect1Width, rect1Height);
	pop();
	showFan(); //background fan
	fill(255);
	text('control with arrow keys', 110, 675);
	text('- avoid getting hit', 110, 690);
	text('level:', 20, 20);
	text(level, 50, 20);
	text('tries:', 20, 40);
	text(tries, 50, 40);
	noStroke();
	movement(); //movement of player
	boundaries(); //boundaries of game plane
	if (level == 1)	{
		checkBall();
		showBall();
	}
	if (level == 2)	{
		showPipe();
	}
	if (level == 3)	{
		startTimer();
		goal = '';
	}
}

function safepoints()	{
	//start
	push();
	rect(50, 350, 100, 700);
	pop();
	//end
	push();
	rect(950, 350, 100, 700);
	noStroke();
	fill(255);
	textAlign(CENTER, CENTER);
	textSize(30);
	text(goal, 950, height/2);
	pop();
	if (level == 1 && rect1X > 900 || level == 2 && rect1X > 900)	{
		rect1X = 50
		rect1Y = 350
		level++;
	}
}

function movement() {
  if (keyIsDown(LEFT_ARROW)) {
    rect1X -= move;
  }

  if (keyIsDown(RIGHT_ARROW)) {
    rect1X += move;
  }

  if (keyIsDown(UP_ARROW)) {
    rect1Y -= move;
  }

  if (keyIsDown(DOWN_ARROW)) {
    rect1Y += move;
  }
}

function  boundaries()	{
	if (rect1X < offset)	{
		rect1X =+ offset;
	}
	if (rect1X > width-offset)	{
		rect1X = width-offset;
	}
	if (rect1Y < offset)	{
		rect1Y =+ offset;
	}
	if (rect1Y > height-offset)	{
		rect1Y = height-offset;
	}
}

function checkBall()	{
	if (ball.length < ballnum)	{
		ball.push(new Ball());
	}
}

function showBall()	{
	for (let i = 0; i < ball.length; i++)	{
		ball[i].show();
		ball[i].move();
		ball[i].collision();
		if (ball[i].y < -25)	{
			ball.splice(i, 1)
		}
	}
}

function showPipe()	{
	if (frameCount % 60 == 0)	{
		pipes.push(new Pipe());
	}
	for (let i = 0; i < pipes.length; i++)	{
		pipes[i].show();
		pipes[i].move();
		pipes[i].collision();
		if (pipes[i].x < 100)	{
			pipes.splice(i, 1)
		}
	}
}

function showFan()	{
	fan.move();
	fan.show();
}

function startTimer()	{
	push();
	textAlign(CENTER, CENTER);
	textSize(25);
	text(content1, width/2, height/2-20);
	text(timer, width/2, height/2+20)
	pop();
	if (frameCount % 60 == 0 && timer > 0)	{
		timer --;
	}
	if (timer == 0)	{
		content1 = '';
		timer = '';
		checkBullet();
		showBullet();
		startCountdown();
	}
}

function checkBullet()	{
	if (bullet.length < bulletnum)	{
		bullet.push(new Bullet());
	}
}

function showBullet()	{
	for (let i = 0; i < bullet.length; i++)	{
		bullet[i].show();
		bullet[i].move();
		bullet[i].collision();
		if (bullet[i].x < -25)	{
			bullet.splice(i, 1)
		}
	}
}

function startCountdown()	{
	text('TIME LEFT:', 20, 60);
	text(countdown, 20, 80);
	if (frameCount % 60 == 0 && countdown > 0)	{
		countdown --;
	}
	if (countdown == 0)	{
		clear();
		noLoop();
		push();
		background(0);
		textAlign(CENTER, CENTER);
		textSize(90);
		fill(random(255), random(255), random(255));
		text('CONGRATULATIONS!', width/2, height/2-40);
		fill(random(255), random(255), random(255));
		text('YOU WON!', width/2, height/2+40);
		textSize(40);
		fill(random(255), random(255), random(255));
		text('TRIES IN TOTAL:', width/2, height/2+120);
		text(tries, width/2, height/2+170);
		pop();
	}
}
