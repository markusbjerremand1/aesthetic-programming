# MiniX7 - rectangles and circles

**What i made:**

For this weeks MiniX, i have created a simple game consisting of three different levels, with varying obstacles/
challenges. You, as the player (or operator), get to control a little rectangle through these levels. The controls
are simple; press the left arroy key to go left, right to go right, and so on. The movement is smooth, so pressing
down on a key continuously will in turn make the rectangle move until you stop pressing the key.

The goal of the game is simple; get from one end (the left side) to the other side (the right side). Underway,
you will have to dodge a few obstacles. Getting hit by one means you will be moved to the starting point (the left
side), where you will have to start the current level from the beginning. This is true for the first two levels,
however, the third and last level is about survival; you have to survive for 30 seconds, dodging obstacles beginning
flung your way. If you get hit, the timer resets to 30, meaning you can only win if you actually make it through the
30 seconds. Throughout the game, a score, called 'tries', will keep track of how many times you have been hit
- so try completing the game without being hit, being hit once, or whatever you like. See how many tries it takes you
to complete the game.

Play the game: https://markusbjerremand1.gitlab.io/aesthetic-programming/minix7/

**How it works:**

There is a total of four different objects that are utilised in the programme; one of them serve merely as a background
decoration, while the other three are objects used as obstacles for the player. These will be explained with a quick rund-down
of each.

![](minix7v1.JPG) 
**Object 1: Ball**

The first object that the player encounters is the 'Ball'-object. This object is used as an obstacle in the first level,
taking the shape of an ellipse. The ball has a random speed, fixed size, random x-position and a fixed y-position (which is important).
The fixed y-position is important, as the object will be made to move up the canvas from the button, the speed being continuously
subtracted from the objects current y-position to make the object move (this.y = this.y - this.speed). The random x-position
means that the object, when being pushed to an array to make more of the same object, will 'spawn' at different x-positions.

In order to make more objects from this 'Ball'-class, the programme checks if the length of the 'ball'-array is longer than
ballnum; a variable with the value of 15. If the the 'ball'-array is not longer, a new 'Ball'-object is pushed to the array;
this means that, until there are 15 'Ball'-objects in the array, it will continually add the object. The ballnum dictates the
number of 'Ball'-objects that can be drawn from the array. In order to draw these, the function showBall uses a for-loop to check
through every instance of the objects in the array, for each executing the 'show' and 'move' function for the object(s). 
Using this process, 15 'Ball'-objects are on the screen, drifting up the canvas from the bottom of it.
In order to ensure that there always are 15 objects on the screen, an if-statement is used, declaring that; if an objects y-position
is less than -25 (if it goes offscreen) it is deleted with the splice function. The array is then checked, seen to be
less than the variable ballnum, and a new object is pushed to the array, then drawn from the array.

Another function in the constructor function for the 'Ball'-class, besides show and move, is the collision function;
this function determines, through an if-statement, if the player has hit an obstacle. If this is true, the player's position
is reset to the starting point. This particular if-statement for collision detection is best suited for rectangle to rectangle collisions, so it is not the most optimal, however, it seems to work fine in the context of the project.

![](minix7v2.JPG)
**Object 2: Pipe**

The second object that the player encounters is the 'Pipe'-object. Used in the second level, it takes the shape of two rectangles, or 'pipes', one sprouting down from the top of the canvas, the other sprouting up from the bottom of it.
This code is primarily (if not 1 to 1), taken directly from Daniel Shiffman's 'flappy bird' Coding Challenge, which can be found
here: https://www.youtube.com/watch?v=cXgA1d_E-jY&t=1158s. 

Not much has been altered; everything down to the collision detection, frameCount function etc. is a replication of his code, only with some minor alterations (such as speed, or randomness regarding the height of the pipes). The major difference in gameplay, of course, being how the player controls the player character.

Also in this code, the act of using the push-method to add objects to an array is utilized, as it was in the previously mentioned object. However, this time the push is being initalized differently; frameCount is used to make a 'Pipe'-object be pushed to the 'pipes'-array everytime 60 frames have passed. The movement of the object, subtracting the declared 'speed', either from an objects x- or y-position, is also used here, in order to make the object move from the right side of the canvas to the left side. Again, splice is used to delete an object that has moved offscreen, or, as in this case, moved to the point where the startingpoint for the player ends, ensuring that this is a safe zone for the player, so they will not be immediately hit once initiating the level.

![](minix7v3.JPG)
**Object 3: Bullet**

The last object that the player will encounter in the game is the 'Bullet'-object. In short, the 'Bullet'-object is almost identical to the 'Ball'-object, the only difference being that they now drift from the right of the canvas to the left of it. Their y-position is random to ensure they are spread out, and not stacked on top of eachother; they have a fixed x-position; speed is now subtracted from their current x-position, making them drift to the right; the collision detection works in the exact same way as it did for the 'Ball'-object.




